1\. Create in `root` folder file `req.cnf` and add next content:
```
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_ca
prompt = no
[req_distinguished_name]
C = US
ST = VA
L = SomeCity
O = MyCompany
OU = MyDivision
CN = 127.0.0.1:3443
[v3_ca]
keyUsage = critical, digitalSignature, keyAgreement
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
IP.1 = 127.0.0.1
```
2\. Execute:
```
openssl req -nodes -sha256 -new -x509 -keyout key.pem -out cert.pem -days 365 -config req.cnf
```
3\. Install dependencies:
```
npm i
```
4\. Run server:
```
npm run start
```