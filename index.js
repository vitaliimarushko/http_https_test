const app = require('express')();
const fs = require('fs');
const https = require('https');
const http = require('http');

const host = '127.0.0.1';
const noSslPort = 3080;
const sslPort = 3443;

const options = {
  key: fs.readFileSync('./key.pem'),
  cert: fs.readFileSync('./cert.pem')
};

app.all('*', (req, res, next) => {
  if (!req.secure) {
    return res.redirect(`https://${host}:${sslPort}`);
  }

  next();
});

app.get('/', (req, res) => {
  res.end('Hello world!');
});

http
  .createServer(app)
  .listen(noSslPort, host, () => {
    console.log(`Run on: http://${host}:${noSslPort}`);
  });

https
  .createServer(options, app)
  .listen(sslPort, host, () => {
    console.log(`Run on: https://${host}:${sslPort}`);
  });
